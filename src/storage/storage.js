import {observable} from 'mobx';

class Storage {

  constructor() {
    this.currentPageNumber = 1;
    this.currentCuratedPhotos = null;
    this.loadedPages = [];

    this.currentPhotoStatistics = null;
    this.loadedStatistics = [];

    this.currentUsersPhotos = null;
    this.loadedUsersPhotos = [];
  }
}

export default observable.object(new Storage());
