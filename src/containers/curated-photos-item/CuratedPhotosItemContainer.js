import React, {Component} from 'react';
import {observer} from 'mobx-react';
import Modal from 'react-modal';
import './CuratedPhotosItemContainer.css';
import {loadPhotoStatistics, loadUsersPhotos} from '../../services/api';
import storage from '../../storage/storage';
import CuratedPhotosItem from '../../components/curated-photos-item/CuratedPhotosItem';
import PhotoDetails from '../../components/photo-details/PhotoDetails';
import AuthorDetails from '../../components/author-details/AuthorDetails';

class CuratedPhotosItemContainer extends Component {

  componentWillMount() {
    this.setState({
      photoModalDisplayed: false,
      authorModalDisplayed: false,
      authorsPhotoModalDisplayed: false,

      authorsPhoto: null
    });
  }

  _onPhotoModalOpenClick() {
    this.setState({
      photoModalDisplayed: true
    });

    loadPhotoStatistics(this.props.curatedPhoto.id);
  }

  _onPhotoModalCloseClick() {
    this.setState({
      photoModalDisplayed: false
    });
  }

  _onAuthorModalOpenClick() {
    this.setState({
      authorModalDisplayed: true
    });

    loadUsersPhotos(this.props.curatedPhoto.user.username, this.props.curatedPhoto.user.total_photos);
  }

  _onAuthorModalCloseClick() {
    this.setState({
      authorModalDisplayed: false
    });
  }

  _onAuthorsPhotoModalOpenClick(photo) {
    this.setState({
      authorsPhotoModalDisplayed: true,
      authorsPhoto: photo
    });

    loadPhotoStatistics(photo.id);
  }

  _onAuthorsPhotoModalCloseClick() {
    this.setState({
      authorsPhotoModalDisplayed: false
    });

    loadPhotoStatistics(this.props.curatedPhoto.id);
  }

  render() {
    const data = this.props.curatedPhoto;
    const statistics = storage.currentPhotoStatistics;
    const user = this.props.curatedPhoto.user;
    const photos = storage.currentUsersPhotos;

    return (
      <div>
        <CuratedPhotosItem curatedPhoto={this.props.curatedPhoto} onPhotoOpen={this._onPhotoModalOpenClick.bind(this)} onAuthorOpen={this._onAuthorModalOpenClick.bind(this)} />
        <Modal isOpen={this.state.photoModalDisplayed} contentLabel='Photo Modal' onRequestClose={() => this._onPhotoModalCloseClick()}>
          <div className='curated-photos-item-container__modal-container'>
            <PhotoDetails data={data} statistics={statistics} onAuthor={this._onAuthorModalOpenClick.bind(this)}/>
            <button className='curated-photos-item-container__modal-close' onClick={() => this._onPhotoModalCloseClick()}>Close</button>
          </div>
        </Modal>
        <Modal isOpen={this.state.authorModalDisplayed} contentLabel='Author Modal' onRequestClose={() => this._onAuthorModalCloseClick()}>
          <div className='curated-photos-item-container__modal-container'>
            <AuthorDetails user={user} photos={photos} onPhotoOpen={this._onAuthorsPhotoModalOpenClick.bind(this)}>
              <Modal isOpen={this.state.authorsPhotoModalDisplayed} contentLabel='Authors Photo Modal' onRequestClose={() => this._onAuthorsPhotoModalCloseClick()}>
                <div className='curated-photos-item-container__modal-container'>
                  <PhotoDetails data={this.state.authorsPhoto} statistics={statistics} onAuthor={this._onAuthorsPhotoModalCloseClick.bind(this)}/>
                  <button className='curated-photos-item-container__modal-close' onClick={() => this._onAuthorsPhotoModalCloseClick()}>Close</button>
                </div>
              </Modal>
            </AuthorDetails>
            <button className='curated-photos-item-container__modal-close' onClick={() => this._onAuthorModalCloseClick()}>Close</button>
          </div>
        </Modal>
      </div>
    );
  }
}

export default observer(CuratedPhotosItemContainer);
