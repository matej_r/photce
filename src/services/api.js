import storage from '../storage/storage';

export function loadPhotos() {
  storage.currentCuratedPhotos = null;

  const index = storage.loadedPages.findIndex((page) => storage.currentPageNumber === page.pageNumber);

  if (index !== -1) {
    storage.currentCuratedPhotos = storage.loadedPages[index].curatedPhotos;
    return;
  }

  const header = {
    Authorization: 'Client-ID f838ebdccfaed42541ba35a654b0086c7c3642bceffb6f1333fc8acd456b6fe8'
  };

  fetch(`https://api.unsplash.com/photos/curated?page=${storage.currentPageNumber}&per_page=20&order_by=popular`, {
    method: 'GET',
    headers: header
  }).then((res) => res.json())
    .then((res) => {
      storage.currentCuratedPhotos = res;
      storage.loadedPages.push({
        pageNumber: storage.currentPageNumber,
        curatedPhotos: storage.currentCuratedPhotos
      });
    });
}


export function loadPhotoStatistics(photoID) {
  storage.currentPhotoStatistics = null;

  const index = storage.loadedStatistics.findIndex((statistics) => photoID === statistics.id);

  if (index !== -1) {
    storage.currentPhotoStatistics = storage.loadedStatistics[index];
    return;
  }

  const header = {
    Authorization: 'Client-ID f838ebdccfaed42541ba35a654b0086c7c3642bceffb6f1333fc8acd456b6fe8'
  };

  fetch(`https://api.unsplash.com/photos/${photoID}/statistics`, {
    method: 'GET',
    headers: header
  }).then((res) => res.json())
    .then((res) => {
      storage.currentPhotoStatistics = res;
      storage.loadedStatistics.push(res);
    });
}

export function loadUsersPhotos(username, numberOfPhotos) {
  storage.currentUsersPhotos = null;

  const index = storage.loadedUsersPhotos.findIndex((usersPhotos) => username === usersPhotos.username);

  if (index !== -1) {
    storage.currentUsersPhotos = storage.loadedUsersPhotos[index].photos;
    return;
  }

  const header = {
    Authorization: 'Client-ID f838ebdccfaed42541ba35a654b0086c7c3642bceffb6f1333fc8acd456b6fe8'
  };

  fetch(`https://api.unsplash.com//users/${username}/photos?per_page=${numberOfPhotos}`, {
    method: 'GET',
    headers: header
  }).then((res) => res.json())
    .then((res) => {
      storage.currentUsersPhotos = res;
      storage.loadedUsersPhotos.push({
        username,
        photos: res
      });
    });
}
