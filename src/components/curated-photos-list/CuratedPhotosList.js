import React from 'react';
import './CuratedPhotosList.css';
import CuratedPhotosItemContainer from '../../containers/curated-photos-item/CuratedPhotosItemContainer';

const CuratedPhotosList = ({curatedPhotos}) => (
  <div className='curated-photos-list__container'>
    {
      curatedPhotos.map((curatedPhoto) => <CuratedPhotosItemContainer curatedPhoto={curatedPhoto} />)
    }
  </div>
);

export default CuratedPhotosList;
