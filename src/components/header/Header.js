import React from 'react';
import './Header.css';
import logo from '../../assets/photce.png';

const Header = () => (
  <div className='header__container'>
    <img src={logo} alt='Photce' />
  </div>
);

export default Header;
