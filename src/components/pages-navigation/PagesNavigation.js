import React from 'react';
import './PagesNavigation.css';

const PagesNavigation = ({currentPageNumber, onPrevious, onFollowing}) => (
  <div className='pages-navigation__container'>
    <button className='pages-navigation__button' onClick={() => onPrevious()} hidden={currentPageNumber === 1}>{'<'}</button>
    <h>{currentPageNumber}</h>
    <button className='pages-navigation__button' onClick={() => onFollowing()}>{'>'}</button>
  </div>
);

export default PagesNavigation;
