import React from 'react';
import './Loading.css';
import loading from '../../assets/loading.gif';

const Loading = () => (
  <div className='loading__container'>
    <img className='loading__gif' src={loading} alt='loading' />
  </div>
);

export default Loading;
