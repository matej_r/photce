import React from 'react';
import './CuratedPhotosItem.css';

const CuratedPhotosItem = ({curatedPhoto, onPhotoOpen, onAuthorOpen}) => (
  <div className='curated-photos-item__container'>
    <img className='curated-photos-item__image' src={curatedPhoto.urls.small} alt={curatedPhoto.description} onClick={() => onPhotoOpen()} />
    <div className='curated-photos-item__info'>
      <p>{curatedPhoto.description}</p>
      <p className='curated-photos-item__author-name' onClick={() => onAuthorOpen()}>{curatedPhoto.user.name}</p>
    </div>
  </div>
);

export default CuratedPhotosItem;
