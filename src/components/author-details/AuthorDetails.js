import React from 'react';
import './AuthorDetails.css';
import Loading from '../loading/Loading';

const AuthorDetails = ({user, photos, onPhotoOpen, children}) => (
  <div className='author-details__container'>
    <img className='author-details__profile-image' src={user.profile_image.large} alt={user.name} />
    <div>
      <p>{user.name}</p>
      <a className='author-details__link' href={user.portfolio_url}><p>{user.portfolio_url}</p></a>
    </div>
    <div className='author-details__photos-container'>
      <p>Photos</p>
      {
        photos === null
          ? <Loading />
          : photos.map((photo) =>
            <div>
              <img className='author-details__photo' src={photo.urls.small} alt={photo.description} onClick={() => onPhotoOpen(photo)}/>
              {children}
            </div>
          )
      }
    </div>
  </div>
);

export default AuthorDetails;
