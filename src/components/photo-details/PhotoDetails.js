import React from 'react';
import './PhotoDetails.css';
import Loading from '../loading/Loading';

const PhotoDetails = ({data, statistics, onAuthor}) => (
  <div className='photo-details__container'>
    <img className='photo-details__photo' src={data.urls.regular} alt={data.description} />
    <div>
      <p>{data.description}</p>
      <p className='photo-details__author' onClick={() => onAuthor()}>{data.user.name}</p>
    </div>
    <div>
      {
        statistics === null
          ? <Loading />
          : <div>
            <h className='photo-details__statistic-data'>Views: {statistics.views.total}</h>
            <h className='photo-details__statistic-data'>Downloads: {statistics.downloads.total}</h>
            <h className='photo-details__statistic-data'>Likes: {statistics.likes.total}</h>
          </div>
      }
    </div>
  </div>
);

export default PhotoDetails;
