import React, {Component} from 'react';
import {observer} from 'mobx-react';
import './App.css';
import storage from '../../storage/storage';
import {loadPhotos} from '../../services/api';
import Header from '../../components/header/Header';
import Loading from '../../components/loading/Loading';
import CuratedPhotosList from '../../components/curated-photos-list/CuratedPhotosList';
import PagesNavigation from '../../components/pages-navigation/PagesNavigation';

class App extends Component {

  componentWillMount() {
    loadPhotos();
  }

  _onPreviousButtonClick() {
    storage.currentPageNumber--;
    loadPhotos();
  }

  _onFollowingButtonClick() {
    storage.currentPageNumber++;
    loadPhotos();
  }

  render() {
    return (
      <div className="app__container">
        <Header />
        {
          storage.currentCuratedPhotos !== null
            ? <CuratedPhotosList curatedPhotos={storage.currentCuratedPhotos} />
            : <Loading />
        }
        <PagesNavigation currentPageNumber={storage.currentPageNumber} onPrevious={this._onPreviousButtonClick} onFollowing={this._onFollowingButtonClick}/>
      </div>
    );
  }
}

export default observer(App);
